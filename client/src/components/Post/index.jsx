import React from 'react';
import PropTypes from 'prop-types';
import { Card, Image, Label, Icon, Grid, Popup } from 'semantic-ui-react';
import moment from 'moment';

import styles from './styles.module.scss';

const Post = ({
    post,
    likePost,
    dislikePost,
    toggleExpandedPost,
    toggleEditedPost,
    sharePost,
    ownPost,
    deletePost,
    reactions,
    getUsersReactions,
    clearUsersReactions,
}) => {
    const {
        id,
        image,
        body,
        user,
        likeCount,
        dislikeCount,
        commentCount,
        createdAt,
    } = post;
    const date = moment(createdAt).fromNow();
    const reactionsList = (<ul>{reactions.map(item => (<li key={item}>{item}</li>))}</ul>);
    return (
        <Card style={{ width: '100%' }}>
            {image && <Image src={image.link} wrapped ui={false} />}
            <Card.Content>
                <Card.Meta>
                    <span className="date">
                        posted by
                        {' '}
                        {user.username}
                        {' - '}
                        {date}
                    </span>
                </Card.Meta>
                <Card.Description>
                    {body}
                </Card.Description>
            </Card.Content>
            <Card.Content extra>
                <Grid columns={2}>
                    <Grid.Row>
                        <Grid.Column>
                            <Popup
                                header={reactions.length ? 'users who liked' : 'nobody liked this post'}
                                content={reactionsList}
                                trigger={(
                                    <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => likePost(id)}>
                                        <Icon name="thumbs up" />
                                        {likeCount}
                                    </Label>
                                )}
                                position="bottom center"
                                on={['hover', 'click']}
                                onOpen={() => getUsersReactions(id, true)}
                                onClose={clearUsersReactions}
                            />
                            <Popup
                                header={reactions.length ? 'users who disliked' : 'nobody disliked this post'}
                                content={reactionsList}
                                trigger={(
                                    <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => dislikePost(id)}>
                                        <Icon name="thumbs down" />
                                        {dislikeCount}
                                    </Label>
                                )}
                                position="bottom center"
                                on={['hover', 'click']}
                                onOpen={() => getUsersReactions(id, false)}
                                onClose={clearUsersReactions}
                            />
                            <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
                                <Icon name="comment" />
                                {commentCount}
                            </Label>
                            <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)}>
                                <Icon name="share alternate" />
                            </Label>
                        </Grid.Column>
                        {ownPost
                        && (
                            <Grid.Column textAlign="right">
                                <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleEditedPost(id)}>
                                    <Icon name="edit" />
                                </Label>
                                <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => deletePost(id)}>
                                    <Icon name="archive" />
                                </Label>
                            </Grid.Column>
                        )}
                    </Grid.Row>
                </Grid>
            </Card.Content>
        </Card>
    );
};


Post.propTypes = {
    post: PropTypes.objectOf(PropTypes.any).isRequired,
    likePost: PropTypes.func.isRequired,
    dislikePost: PropTypes.func.isRequired,
    toggleExpandedPost: PropTypes.func.isRequired,
    toggleEditedPost: PropTypes.func.isRequired,
    sharePost: PropTypes.func.isRequired,
    ownPost: PropTypes.bool.isRequired,
    deletePost: PropTypes.func,
    reactions: PropTypes.arrayOf(PropTypes.string),
    getUsersReactions: PropTypes.func,
    clearUsersReactions: PropTypes.func,
};

Post.defaultProps = {
    deletePost: () => ({}),
    reactions: [],
    getUsersReactions: () => ({}),
    clearUsersReactions: () => ({}),
};

export default Post;

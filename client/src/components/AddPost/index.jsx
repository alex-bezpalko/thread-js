import React from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Icon, Image, Segment } from 'semantic-ui-react';

import styles from './styles.module.scss';

const initialState = {
    body: '',
    imageId: undefined,
    imageLink: undefined
};

class AddEditPost extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            ...initialState,
            isUploading: false
        };
    }

    componentDidMount() {
        const { post } = this.props;
        if (post) {
            const { body } = post;
            this.setState({ body });
        }
    }

    handleAddPost = async () => {
        const { post, closeModal } = this.props;
        const { imageId, body } = this.state;
        if (!body) {
            return;
        }
        if (post) {
            const { id } = post;
            await this.props.updatePost(id, { imageId, body });
            closeModal();
        } else {
            await this.props.addPost({ imageId, body });
            this.setState(initialState);
        }
    }

    handleUploadFile = async ({ target }) => {
        this.setState({ isUploading: true });
        try {
            const { id: imageId, link: imageLink } = await this.props.uploadImage(target.files[0]);
            this.setState({ imageId, imageLink, isUploading: false });
        } catch {
            // TODO: show error
            this.setState({ isUploading: false });
        }
    }

    render() {
        const { imageLink, body, isUploading } = this.state;
        const { post } = this.props;
        return (
            <Segment>
                <Form onSubmit={this.handleAddPost}>
                    <Form.TextArea
                        name="body"
                        value={body}
                        placeholder="What is the news?"
                        onChange={ev => this.setState({ body: ev.target.value })}
                    />
                    {imageLink && (
                        <div className={styles.imageWrapper}>
                            <Image className={styles.image} src={imageLink} alt="post" />
                        </div>
                    )}
                    <Button color="teal" icon labelPosition="left" as="label" loading={isUploading} disabled={isUploading}>
                        <Icon name="image" />
                        {post && post.image ? 'Change image' : 'Attach image'}
                        <input name="image" type="file" onChange={this.handleUploadFile} hidden />
                    </Button>
                    <Button floated="right" color="blue" type="submit">{post ? 'Change Post' : 'Post'}</Button>
                </Form>
            </Segment>
        );
    }
}

AddEditPost.propTypes = {
    post: PropTypes.objectOf(PropTypes.any),
    addPost: PropTypes.func,
    uploadImage: PropTypes.func.isRequired,
    updatePost: PropTypes.func,
    closeModal: PropTypes.func,
};

AddEditPost.defaultProps = {
    post: undefined,
    addPost: () => ({}),
    updatePost: () => ({}),
    closeModal: () => ({})
};

export default AddEditPost;

import React from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Icon, Popup } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';

import styles from './styles.module.scss';

const Comment = (props) => {
    const {
        comment: { id, body, createdAt, user, likeCount, dislikeCount, postId },
        ownComment,
        reactComment,
        editComment,
        deleteComment,
        reactions,
        getUsersReactions,
        clearUsersReactions,
    } = props;
    const date = moment(createdAt).fromNow();
    const reactionsList = (<ul>{reactions.map(item => (<li key={item}>{item}</li>))}</ul>);
    return (
        <CommentUI className={styles.comment}>
            <CommentUI.Avatar src={getUserImgLink(user.image)} />
            <CommentUI.Content>
                <CommentUI.Author as="a">
                    {user.username}
                </CommentUI.Author>
                <CommentUI.Metadata>
                    {date}
                </CommentUI.Metadata>
                <CommentUI.Text>
                    {body}
                </CommentUI.Text>
                <CommentUI.Actions>
                    <div style={{ display: 'flex' }}>
                        <div style={{ flex: '1 1' }}>
                            <Popup
                                header={reactions.length ? 'users who liked' : 'nobody liked this comment'}
                                content={reactionsList}
                                trigger={(
                                    <CommentUI.Action onClick={() => reactComment(id, true)}>
                                        <Icon name="thumbs up" />
                                        {likeCount}
                                    </CommentUI.Action>
                                )}
                                position="bottom center"
                                on={['hover', 'click']}
                                onOpen={() => getUsersReactions(id, true)}
                                onClose={clearUsersReactions}
                            />
                            <Popup
                                header={reactions.length ? 'users who disliked' : 'nobody disliked this comment'}
                                content={reactionsList}
                                trigger={(
                                    <CommentUI.Action onClick={() => reactComment(id, false)}>
                                        <Icon name="thumbs down" />
                                        {dislikeCount}
                                    </CommentUI.Action>
                                )}
                                position="bottom center"
                                on={['hover', 'click']}
                                onOpen={() => getUsersReactions(id, false)}
                                onClose={clearUsersReactions}
                            />
                        </div>
                        {ownComment
                            && (
                                <div style={{ flex: '1 1', textAlign: 'right' }}>
                                    <CommentUI.Action onClick={() => editComment(id, body)}>
                                        <Icon name="edit" />
                                    </CommentUI.Action>
                                    <CommentUI.Action onClick={() => deleteComment(id, postId)}>
                                        <Icon name="archive" />
                                    </CommentUI.Action>
                                </div>
                            )}
                    </div>
                </CommentUI.Actions>
            </CommentUI.Content>
        </CommentUI>
    );
};

Comment.propTypes = {
    comment: PropTypes.objectOf(PropTypes.any).isRequired,
    ownComment: PropTypes.bool.isRequired,
    reactComment: PropTypes.func.isRequired,
    editComment: PropTypes.func.isRequired,
    deleteComment: PropTypes.func.isRequired,
    reactions: PropTypes.arrayOf(PropTypes.string),
    getUsersReactions: PropTypes.func,
    clearUsersReactions: PropTypes.func,
};

Comment.defaultProps = {
    reactions: [],
    getUsersReactions: () => ({}),
    clearUsersReactions: () => ({}),
};

export default Comment;

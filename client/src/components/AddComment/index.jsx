import React from 'react';
import PropTypes from 'prop-types';
import { Form, Button } from 'semantic-ui-react';
import _ from 'lodash';

class AddComment extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            body: ''
        };
    }

    componentDidUpdate(prevProps) {
        if (_.get(prevProps, 'comment.id') !== _.get(this.props, 'comment.id')) {
            const body = _.get(this.props, 'comment.body', '');
            this.setBody(body);
        }
    }

    setBody = (body) => {
        this.setState({ body });
    }

    handleAddComment = async () => {
        const { body } = this.state;
        const { comment, updateComment } = this.props;
        if (!body) {
            return;
        }
        if (comment) {
            await updateComment(comment.id, { body });
        } else {
            const { postId } = this.props;
            await this.props.addComment({ postId, body });
        }
        this.setState({ body: '' });
    }

    render() {
        const { body } = this.state;
        const { comment, cancelUpdate } = this.props;
        return (
            <Form reply onSubmit={this.handleAddComment}>
                <Form.TextArea
                    value={body}
                    placeholder="Type a comment..."
                    onChange={ev => this.setState({ body: ev.target.value })}
                />
                <Button type="submit" content={comment ? 'Change comment' : 'Post comment'} labelPosition="left" icon="edit" primary />
                {comment && (
                    <Button type="button" onClick={cancelUpdate} content="Cancel" labelPosition="left" icon="cancel" color="red" />
                )}
            </Form>
        );
    }
}

AddComment.propTypes = {
    addComment: PropTypes.func.isRequired,
    postId: PropTypes.string.isRequired,
    comment: PropTypes.objectOf({
        id: PropTypes.number,
        body: PropTypes.string,
    }),
    updateComment: PropTypes.func.isRequired,
    cancelUpdate: PropTypes.func.isRequired,
};

AddComment.defaultProps = {
    comment: null,
};

export default AddComment;

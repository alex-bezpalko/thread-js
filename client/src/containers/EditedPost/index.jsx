import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal } from 'semantic-ui-react';
import AddPost from 'src/components/AddPost';
import Spinner from 'src/components/Spinner';
import * as imageService from 'src/services/imageService';
import { toggleEditedPost, updatePost } from 'src/containers/Thread/actions';

class EditedPost extends React.Component {
    state = {
        open: true
    };

    closeModal = () => {
        this.props.toggleEditedPost();
    }

    uploadImage = file => imageService.uploadImage(file);

    render() {
        const { post, ...props } = this.props;
        return (
            <Modal dimmer="blurring" centered={false} open={this.state.open} onClose={this.closeModal}>
                {post
                    ? (
                        <Modal.Content>
                            <AddPost
                                post={post}
                                updatePost={props.updatePost}
                                closeModal={this.closeModal}
                                uploadImage={this.uploadImage}
                            />
                        </Modal.Content>
                    )
                    : <Spinner />
                }
            </Modal>
        );
    }
}

EditedPost.propTypes = {
    post: PropTypes.objectOf(PropTypes.any).isRequired,
    updatePost: PropTypes.func.isRequired,
    toggleEditedPost: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
    post: rootState.posts.editedPost
});
const actions = { toggleEditedPost, updatePost };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(EditedPost);

import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as imageService from 'src/services/imageService';
import ExpandedPost from 'src/containers/ExpandedPost';
import EditedPost from 'src/containers/EditedPost';
import Post from 'src/components/Post';
import AddPost from 'src/components/AddPost';
import SharedPostLink from 'src/components/SharedPostLink';
import { Checkbox, Loader } from 'semantic-ui-react';
import InfiniteScroll from 'react-infinite-scroller';
import {
    loadPosts,
    loadMorePosts,
    likePost,
    dislikePost,
    toggleExpandedPost,
    toggleEditedPost,
    addPost,
    deletePost,
    getUsersReactionsForPost,
} from './actions';

import styles from './styles.module.scss';

class Thread extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sharedPostId: undefined,
            showOwnPosts: false,
            showNotOwnPosts: false,
            showFavoritePosts: false,
            usersReactions: [],
        };
        this.postsFilter = {
            userId: undefined,
            filterType: '',
            from: 0,
            count: 10
        };
    }

    tooglePosts = (filterType) => {
        this.setState(
            ({ showOwnPosts, showNotOwnPosts, showFavoritePosts }) => {
                if (filterType === 'own') {
                    return {
                        showOwnPosts: !showOwnPosts,
                        showNotOwnPosts: false,
                        showFavoritePosts: false,
                    };
                }
                if (filterType === 'notOwn') {
                    return {
                        showNotOwnPosts: !showNotOwnPosts,
                        showOwnPosts: false,
                        showFavoritePosts: false,
                    };
                }
                if (filterType === 'favorite') {
                    return {
                        showFavoritePosts: !showFavoritePosts,
                        showOwnPosts: false,
                        showNotOwnPosts: false,
                    };
                }
                return {
                    showFavoritePosts: false,
                    showOwnPosts: false,
                    showNotOwnPosts: false,
                };
            },
            () => {
                Object.assign(this.postsFilter, {
                    userId: this.state.showOwnPosts || this.state.showNotOwnPosts || this.state.showFavoritePosts
                        ? this.props.userId
                        : undefined,
                    filterType,
                    from: 0
                });
                this.props.loadPosts(this.postsFilter);
                this.postsFilter.from = this.postsFilter.count; // for next scroll
            }
        );
    };

    loadMorePosts = () => {
        this.props.loadMorePosts(this.postsFilter);
        const { from, count } = this.postsFilter;
        this.postsFilter.from = from + count;
    }

    sharePost = (sharedPostId) => {
        this.setState({ sharedPostId });
    };

    closeSharePost = () => {
        this.setState({ sharedPostId: undefined });
    }

    uploadImage = file => imageService.uploadImage(file);

    getUsersReactions = async (id, isLike) => {
        const usersReactions = await this.props.getUsersReactionsForPost(id, isLike);
        this.setState({ usersReactions });
    }

    clearUsersReactions = () => {
        this.setState({ usersReactions: [] });
    }

    render() {
        const { posts = [], expandedPost, editedPost, hasMorePosts, userId, ...props } = this.props;
        const { showOwnPosts, sharedPostId, showNotOwnPosts, showFavoritePosts, usersReactions } = this.state;
        return (
            <div className={styles.threadContent}>
                <div className={styles.addPostForm}>
                    <AddPost addPost={props.addPost} uploadImage={this.uploadImage} />
                </div>
                <div className={styles.toolbar}>
                    <Checkbox toggle label="Show only my posts" checked={showOwnPosts} onChange={() => this.tooglePosts('own')} />
                </div>
                <div className={styles.toolbar}>
                    <Checkbox toggle label="Show without my posts" checked={showNotOwnPosts} onChange={() => this.tooglePosts('notOwn')} />
                </div>
                <div className={styles.toolbar}>
                    <Checkbox toggle label="Show favorite posts" checked={showFavoritePosts} onChange={() => this.tooglePosts('favorite')} />
                </div>
                <InfiniteScroll
                    pageStart={0}
                    loadMore={this.loadMorePosts}
                    hasMore={hasMorePosts}
                    loader={<Loader active inline="centered" key={0} />}
                >
                    {posts.map(post => (
                        <Post
                            post={post}
                            likePost={props.likePost}
                            dislikePost={props.dislikePost}
                            toggleExpandedPost={props.toggleExpandedPost}
                            toggleEditedPost={props.toggleEditedPost}
                            sharePost={this.sharePost}
                            ownPost={post.user.id === userId}
                            deletePost={props.deletePost}
                            key={post.id}
                            getUsersReactions={this.getUsersReactions}
                            clearUsersReactions={this.clearUsersReactions}
                            reactions={usersReactions}
                        />
                    ))}
                </InfiniteScroll>
                {
                    expandedPost
                    && <ExpandedPost sharePost={this.sharePost} />
                }
                {
                    editedPost
                    && <EditedPost />
                }
                {
                    sharedPostId
                    && <SharedPostLink postId={sharedPostId} close={this.closeSharePost} />
                }
            </div>
        );
    }
}

Thread.propTypes = {
    posts: PropTypes.arrayOf(PropTypes.object),
    hasMorePosts: PropTypes.bool,
    expandedPost: PropTypes.objectOf(PropTypes.any),
    editedPost: PropTypes.objectOf(PropTypes.any),
    sharedPostId: PropTypes.string,
    userId: PropTypes.string,
    loadPosts: PropTypes.func.isRequired,
    loadMorePosts: PropTypes.func.isRequired,
    likePost: PropTypes.func.isRequired,
    dislikePost: PropTypes.func.isRequired,
    toggleExpandedPost: PropTypes.func.isRequired,
    toggleEditedPost: PropTypes.func.isRequired,
    addPost: PropTypes.func.isRequired,
    deletePost: PropTypes.func.isRequired,
    getUsersReactionsForPost: PropTypes.func.isRequired,
};

Thread.defaultProps = {
    posts: [],
    hasMorePosts: true,
    expandedPost: undefined,
    editedPost: undefined,
    sharedPostId: undefined,
    userId: undefined
};

const mapStateToProps = rootState => ({
    posts: rootState.posts.posts,
    hasMorePosts: rootState.posts.hasMorePosts,
    expandedPost: rootState.posts.expandedPost,
    editedPost: rootState.posts.editedPost,
    userId: rootState.profile.user.id
});

const actions = {
    loadPosts,
    loadMorePosts,
    likePost,
    dislikePost,
    toggleExpandedPost,
    toggleEditedPost,
    addPost,
    deletePost,
    getUsersReactionsForPost,
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Thread);

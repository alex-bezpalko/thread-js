import _ from 'lodash';
import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
import {
    ADD_POST,
    UPDATE_POST,
    DELETE_POST,
    LOAD_MORE_POSTS,
    SET_ALL_POSTS,
    SET_EXPANDED_POST,
    SET_EDITED_POST
} from './actionTypes';

const setPostsAction = posts => ({
    type: SET_ALL_POSTS,
    posts
});

const addMorePostsAction = posts => ({
    type: LOAD_MORE_POSTS,
    posts
});

const addPostAction = post => ({
    type: ADD_POST,
    post
});

const updatePostAction = post => ({
    type: UPDATE_POST,
    post
});

const deletePostAction = id => ({
    type: DELETE_POST,
    id
});

const setExpandedPostAction = post => ({
    type: SET_EXPANDED_POST,
    post
});

const setEditedPostAction = post => ({
    type: SET_EDITED_POST,
    post
});

export const loadPosts = filter => async (dispatch) => {
    const posts = await postService.getAllPosts(filter);
    dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
    const { posts: { posts } } = getRootState();
    const loadedPosts = await postService.getAllPosts(filter);
    const filteredPosts = loadedPosts
        .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));
    dispatch(addMorePostsAction(filteredPosts));
};

export const applyPost = postId => async (dispatch) => {
    const post = await postService.getPost(postId);
    dispatch(addPostAction(post));
};

export const addPost = post => async (dispatch) => {
    const { id } = await postService.addPost(post);
    const newPost = await postService.getPost(id);
    dispatch(addPostAction(newPost));
};

export const updatePost = (id, post) => async (dispatch) => {
    await postService.updatePost(id, post);
    const newPost = await postService.getPost(id);
    dispatch(updatePostAction(newPost));
};

export const deletePost = id => async (dispatch) => {
    await postService.deletePost(id);
    dispatch(deletePostAction(id));
};

export const toggleExpandedPost = postId => async (dispatch) => {
    const post = postId ? await postService.getPost(postId) : undefined;
    dispatch(setExpandedPostAction(post));
};

export const toggleEditedPost = postId => async (dispatch) => {
    const post = postId ? await postService.getPost(postId) : undefined;
    dispatch(setEditedPostAction(post));
};

export const likePost = postId => async (dispatch, getRootState) => {
    const { id } = await postService.likePost(postId);
    const diff = id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed

    const { dislikeCount } = await postService.getPost(postId);

    const mapLikes = post => ({
        ...post,
        dislikeCount,
        likeCount: Number(post.likeCount) + diff // diff is taken from the current closure
    });

    const { posts: { posts, expandedPost } } = getRootState();
    const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));

    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost.id === postId) {
        dispatch(setExpandedPostAction(mapLikes(expandedPost)));
    }
};

export const dislikePost = postId => async (dispatch, getRootState) => {
    const { id } = await postService.dislikePost(postId);
    const diff = id ? 1 : -1;

    const { likeCount } = await postService.getPost(postId);

    const mapDislikes = post => ({
        ...post,
        likeCount,
        dislikeCount: Number(post.dislikeCount) + diff
    });

    const { posts: { posts, expandedPost } } = getRootState();
    const updated = posts.map(post => (post.id !== postId ? post : mapDislikes(post)));

    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost.id === postId) {
        dispatch(setExpandedPostAction(mapDislikes(expandedPost)));
    }
};

export const addComment = request => async (dispatch, getRootState) => {
    const { id } = await commentService.addComment(request);
    const comment = await commentService.getComment(id);

    const mapComments = post => ({
        ...post,
        commentCount: Number(post.commentCount) + 1,
        comments: [...(post.comments || []), comment] // comment is taken from the current closure
    });

    const { posts: { posts, expandedPost } } = getRootState();
    const updated = posts.map(post => (post.id !== comment.postId
        ? post
        : mapComments(post)));

    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost.id === comment.postId) {
        dispatch(setExpandedPostAction(mapComments(expandedPost)));
    }
};

export const reactComment = (commentId, isLike) => async (dispatch, getRootState) => {
    const { ...comment } = await commentService.reactComment(commentId, isLike);

    const { posts: { expandedPost } } = getRootState();
    const { comments } = expandedPost;

    const updated = comments.map(item => (item.id !== commentId ? item : comment));

    dispatch(setExpandedPostAction({ ...expandedPost, comments: updated }));
};

export const updateComment = (id, request) => async (dispatch, getRootState) => {
    const comment = await commentService.updateComment(id, request);

    const mapComments = post => ({
        ...post,
        comments: post.comments.map(item => (item.id !== comment.id ? item : { ...item, ...comment }))
    });

    const { posts: { expandedPost } } = getRootState();

    if (expandedPost && expandedPost.id === comment.postId) {
        dispatch(setExpandedPostAction(mapComments(expandedPost)));
    }
};

export const deleteComment = (id, postId) => async (dispatch, getRootState) => {
    await commentService.deleteComment(id);

    const { posts: { posts, expandedPost } } = getRootState();

    const updated = posts.map(post => (post.id !== postId
        ? post
        : { ...post, commentCount: Number(post.commentCount) - 1 }));

    dispatch(setPostsAction(updated));

    const comments = expandedPost.comments.filter(item => (item.id !== id));
    dispatch(setExpandedPostAction({ ...expandedPost, commentCount: Number(expandedPost.commentCount) - 1, comments }));
};

export const getUsersReactionsForPost = (id, isLike) => async () => {
    const reactions = await postService.getUsersReactions(id, isLike);

    return reactions.postReactions.map(item => _.get(item, 'user.username', undefined)).filter(item => item !== undefined);
};

export const getUsersReactionsForComment = (id, isLike) => async () => {
    const reactions = await commentService.getUsersReactions(id, isLike);

    return reactions.commentReactions.map(item => _.get(item, 'user.username', undefined)).filter(item => item !== undefined);
};

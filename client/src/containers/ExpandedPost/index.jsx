import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal, Comment as CommentUI, Header } from 'semantic-ui-react';
import moment from 'moment';
import {
    likePost,
    dislikePost,
    toggleExpandedPost,
    addComment,
    reactComment,
    updateComment,
    deleteComment,
    getUsersReactionsForPost,
    getUsersReactionsForComment,
} from 'src/containers/Thread/actions';
import Post from 'src/components/Post';
import Comment from 'src/components/Comment';
import AddComment from 'src/components/AddComment';
import Spinner from 'src/components/Spinner';

class ExpandedPost extends React.Component {
    state = {
        open: true,
        editedComment: null,
        usersReactions: [],
    };

    closeModal = () => {
        this.props.toggleExpandedPost();
    }

    setEditedComment = (id, body) => {
        this.setState({ editedComment: { id, body } });
    }

    updateComment = (id, request) => {
        this.setState({ editedComment: null });
        this.props.updateComment(id, request);
    }

    cancelEditedComment = () => {
        this.setState({ editedComment: null });
    }

    getUsersReactionsForPost = async (id, isLike) => {
        const usersReactions = await this.props.getUsersReactionsForPost(id, isLike);
        this.setState({ usersReactions });
    }

    getUsersReactionsForComment = async (id, isLike) => {
        const usersReactions = await this.props.getUsersReactionsForComment(id, isLike);
        this.setState({ usersReactions });
    }

    clearUsersReactions = () => {
        this.setState({ usersReactions: [] });
    }

    render() {
        const { post, sharePost, ...props } = this.props;
        const { editedComment, usersReactions } = this.state;
        return (
            <Modal dimmer centered={false} open={this.state.open} onClose={this.closeModal}>
                {post
                    ? (
                        <Modal.Content>
                            <Post
                                post={post}
                                likePost={props.likePost}
                                dislikePost={props.dislikePost}
                                toggleExpandedPost={props.toggleExpandedPost}
                                sharePost={sharePost}
                                getUsersReactions={this.getUsersReactionsForPost}
                                clearUsersReactions={this.clearUsersReactions}
                                reactions={usersReactions}
                            />
                            <CommentUI.Group style={{ maxWidth: '100%' }}>
                                <Header as="h3" dividing>
                                    Comments
                                </Header>
                                {post.comments && post.comments
                                    .sort((c1, c2) => moment(c1.createdAt).diff(c2.createdAt))
                                    .map(comment => (
                                        <Comment
                                            key={comment.id}
                                            comment={comment}
                                            ownComment={comment.user.id === props.userId}
                                            reactComment={props.reactComment}
                                            editComment={this.setEditedComment}
                                            deleteComment={props.deleteComment}
                                            getUsersReactions={this.getUsersReactionsForComment}
                                            clearUsersReactions={this.clearUsersReactions}
                                            reactions={usersReactions}
                                        />
                                    ))
                                }
                                <AddComment
                                    postId={post.id}
                                    addComment={props.addComment}
                                    comment={editedComment}
                                    updateComment={this.updateComment}
                                    cancelUpdate={this.cancelEditedComment}
                                />
                            </CommentUI.Group>
                        </Modal.Content>
                    )
                    : <Spinner />
                }
            </Modal>
        );
    }
}

ExpandedPost.propTypes = {
    post: PropTypes.objectOf(PropTypes.any).isRequired,
    toggleExpandedPost: PropTypes.func.isRequired,
    likePost: PropTypes.func.isRequired,
    dislikePost: PropTypes.func.isRequired,
    addComment: PropTypes.func.isRequired,
    sharePost: PropTypes.func.isRequired,
    userId: PropTypes.string,
    reactComment: PropTypes.func.isRequired,
    updateComment: PropTypes.func.isRequired,
    deleteComment: PropTypes.func.isRequired,
    getUsersReactionsForPost: PropTypes.func.isRequired,
    getUsersReactionsForComment: PropTypes.func.isRequired,
};

ExpandedPost.defaultProps = {
    userId: undefined
};

const mapStateToProps = rootState => ({
    post: rootState.posts.expandedPost,
    userId: rootState.profile.user.id
});
const actions = {
    likePost,
    dislikePost,
    toggleExpandedPost,
    addComment,
    reactComment,
    updateComment,
    deleteComment,
    getUsersReactionsForPost,
    getUsersReactionsForComment,
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ExpandedPost);

import callWebApi from 'src/helpers/webApiHelper';

export const addComment = async (request) => {
    const response = await callWebApi({
        endpoint: '/api/comments',
        type: 'POST',
        request
    });
    return response.json();
};

export const getComment = async (id) => {
    const response = await callWebApi({
        endpoint: `/api/comments/${id}`,
        type: 'GET'
    });
    return response.json();
};

export const reactComment = async (commentId, isLike) => {
    const response = await callWebApi({
        endpoint: '/api/comments/react',
        type: 'PUT',
        request: {
            commentId,
            isLike
        }
    });
    return response.json();
};

export const updateComment = async (id, request) => {
    const response = await callWebApi({
        endpoint: `/api/comments/${id}`,
        type: 'PUT',
        request
    });
    return response.json();
};

export const deleteComment = async (id) => {
    await callWebApi({
        endpoint: `/api/comments/${id}`,
        type: 'DELETE'
    });
};

export const getUsersReactions = async (commentId, isLike) => {
    const response = await callWebApi({
        endpoint: `/api/comments/getReact/${commentId}`,
        type: 'GET',
        query: { isLike }
    });
    return response.json();
};

import commentRepository from '../../data/repositories/comment.repository';
import commentReactionRepository from '../../data/repositories/comment-reaction.repository';

export const create = (userId, comment) => commentRepository.create({
    ...comment,
    userId
});

export const getCommentById = id => commentRepository.getCommentById(id);

export const update = (id, comment) => commentRepository.updateById(
    id,
    { ...comment }
);

export const remove = id => commentRepository.deleteById(id);

export const setReaction = async (userId, { commentId, isLike = true }) => {
    const updateOrDelete = react => (react.isLike === isLike
        ? commentReactionRepository.deleteById(react.id)
        : commentReactionRepository.updateById(react.id, { isLike }));

    const reaction = await commentReactionRepository.getCommentReaction(userId, commentId);

    if (reaction) {
        await updateOrDelete(reaction);
    } else {
        await commentReactionRepository.create({ userId, commentId, isLike });
    }

    return commentRepository.getCommentById(commentId);
};

export const getUsersReactionsForComment = (id, filter) => commentRepository.getUsersReactionsForComment(id, filter);

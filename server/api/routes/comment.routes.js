import { Router } from 'express';
import * as commentService from '../services/comment.service';

const router = Router();

router
    .get('/:id', (req, res, next) => commentService.getCommentById(req.params.id)
        .then(comment => res.send(comment))
        .catch(next))
    .post('/', (req, res, next) => commentService.create(req.user.id, req.body) // user added to the request in the jwt strategy, see passport config
        .then(comment => res.send(comment))
        .catch(next))
    .get('/getReact/:id', (req, res, next) => commentService.getUsersReactionsForComment(req.params.id, req.query)
        .then(users => res.send(users))
        .catch(next))
    .put('/react', (req, res, next) => commentService.setReaction(req.user.id, req.body)
        .then(reaction => res.send(reaction))
        .catch(next))
    .put('/:id', (req, res, next) => commentService.update(req.params.id, req.body)
        .then(post => res.send(post))
        .catch(next))
    .delete('/:id', (req, res, next) => commentService.remove(req.params.id)
        .then(() => res.sendStatus('204'))
        .catch(next));

export default router;

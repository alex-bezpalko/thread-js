import { CommentModel, UserModel, ImageModel, CommentReactionModel } from '../models/index';
import BaseRepository from './base.repository';
import sequelize from '../db/connection';

class CommentRepository extends BaseRepository {
    getCommentById(id) {
        return this.model.findOne({
            group: [
                'comment.id',
                'user.id',
                'user->image.id'
            ],
            where: { id },
            attributes: {
                include: [
                    [sequelize.literal(`
                        (SELECT COUNT("commentReaction"."commentId")
                        FROM "commentReactions" as "commentReaction"
                        WHERE "comment"."id" = "commentReaction"."commentId" AND "commentReaction"."isLike")`), 'likeCount'],
                    [sequelize.literal(`
                        (SELECT COUNT("commentReaction"."commentId")
                        FROM "commentReactions" as "commentReaction"
                        WHERE "comment"."id" = "commentReaction"."commentId" AND "commentReaction"."isLike" = false)`), 'dislikeCount'],
                ],
            },
            include: [{
                model: UserModel,
                attributes: ['id', 'username'],
                include: {
                    model: ImageModel,
                    attributes: ['id', 'link']
                }
            }]
        });
    }

    async getUsersReactionsForComment(id, filter) {
        const { isLike } = filter;

        return this.model.findOne({
            where: { id },
            include: {
                model: CommentReactionModel,
                attributes: ['commentId', 'userId'],
                where: {
                    isLike,
                },
                required: false,
                include: {
                    model: UserModel,
                    attributes: ['id', 'username'],
                    required: false
                }
            },
        });
    }
}

export default new CommentRepository(CommentModel);

import Sequelize from 'sequelize';
import sequelize from '../db/connection';
import { PostModel, CommentModel, UserModel, ImageModel, PostReactionModel } from '../models/index';
import BaseRepository from './base.repository';

const { Op } = Sequelize;

const likeCase = bool => `CASE WHEN "postReactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;

class PostRepository extends BaseRepository {
    async getPosts(filter) {
        const {
            from: offset,
            count: limit,
            userId,
            filterType
        } = filter;

        const where = {};
        if (userId) {
            if (filterType === 'own') {
                Object.assign(where, { userId });
            } else if (filterType === 'notOwn') {
                Object.assign(where, { userId: { [Op.ne]: userId } });
            } else if (filterType === 'favorite') {
                Object.assign(where, { id: { [Op.in]:
                    sequelize.literal(`
                        (SELECT "postReaction"."postId"
                        FROM "postReactions" as "postReaction"
                        WHERE "post"."id" = "postReaction"."postId" AND "postReaction"."isLike" AND "postReaction"."userId" = ${userId})`) } });
            }
        }

        return this.model.findAll({
            where,
            attributes: {
                include: [
                    [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId" AND "comment"."deletedAt" IS NULL)`), 'commentCount'],
                    [sequelize.fn('SUM', sequelize.literal(likeCase(true))), 'likeCount'],
                    [sequelize.fn('SUM', sequelize.literal(likeCase(false))), 'dislikeCount']
                ]
            },
            include: [{
                model: ImageModel,
                attributes: ['id', 'link']
            }, {
                model: UserModel,
                attributes: ['id', 'username'],
                include: {
                    model: ImageModel,
                    attributes: ['id', 'link']
                }
            }, {
                model: PostReactionModel,
                attributes: [],
                duplicating: false
            }],
            group: [
                'post.id',
                'image.id',
                'user.id',
                'user->image.id'
            ],
            order: [['createdAt', 'DESC']],
            offset,
            limit
        });
    }

    getPostById(id) {
        return this.model.findOne({
            group: [
                'post.id',
                'comments.id',
                'comments->user.id',
                'comments->user->image.id',
                'user.id',
                'user->image.id',
                'image.id'
            ],
            where: { id },
            attributes: {
                include: [
                    [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId" AND "comment"."deletedAt" IS NULL)`), 'commentCount'],
                    [sequelize.fn('SUM', sequelize.literal(likeCase(true))), 'likeCount'],
                    [sequelize.fn('SUM', sequelize.literal(likeCase(false))), 'dislikeCount']
                ]
            },
            include: [{
                model: CommentModel,
                attributes: {
                    include: [
                        [sequelize.literal(`
                            (SELECT COUNT("commentReaction"."commentId")
                            FROM "commentReactions" as "commentReaction"
                            WHERE "comments"."id" = "commentReaction"."commentId" AND "commentReaction"."isLike")`), 'likeCount'],
                        [sequelize.literal(`
                            (SELECT COUNT("commentReaction"."commentId")
                            FROM "commentReactions" as "commentReaction"
                            WHERE "comments"."id" = "commentReaction"."commentId" AND "commentReaction"."isLike" = false)`), 'dislikeCount'],
                    ]
                },
                include: {
                    model: UserModel,
                    attributes: ['id', 'username'],
                    include: {
                        model: ImageModel,
                        attributes: ['id', 'link']
                    }
                }
            }, {
                model: UserModel,
                attributes: ['id', 'username'],
                include: {
                    model: ImageModel,
                    attributes: ['id', 'link']
                }
            }, {
                model: ImageModel,
                attributes: ['id', 'link']
            }, {
                model: PostReactionModel,
                attributes: []
            }]
        });
    }

    async getUsersReactionsForPost(id, filter) {
        const { isLike } = filter;

        return this.model.findOne({
            where: { id },
            include: {
                model: PostReactionModel,
                attributes: ['postId', 'userId'],
                where: {
                    isLike,
                },
                required: false,
                include: {
                    model: UserModel,
                    attributes: ['id', 'username'],
                    required: false
                }
            },
        });
    }
}

export default new PostRepository(PostModel);
